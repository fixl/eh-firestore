// Copyright (c) 2021 - The Event Horizon Cloud Firestore authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package firestore

import (
	"testing"

	"github.com/google/uuid"
)

type testStruct struct {
	ID     uuid.UUID
	Name   string
	Nested testStruct1
}

type testStruct1 struct {
	ID   uuid.UUID
	Name string
}

func TestMapToFirestore(t *testing.T) {
	in := &testStruct{}

	out, err := toFirestore(in)
	if err != nil {
		t.Errorf("Should not return error: %v", err)
	}

	if _, ok := out["ID"].(string); !ok {
		t.Error("ID should be string")
	}

	if _, ok := out["Nested"].(map[string]interface{})["ID"].(string); !ok {
		t.Errorf("nested id should be string")
	}

	_, err = toFirestore("Fail")
	if err == nil {
		t.Errorf("Should return error")
	}
}

func TestFromFirestore(t *testing.T) {
	uuid1 := uuid.New()
	uuid2 := uuid.New()
	in := map[string]interface{}{
		"id":   uuid1.String(),
		"name": "foo",
		"nested": map[string]interface{}{
			"id":   uuid2.String(),
			"name": "bar",
		},
	}

	var out testStruct

	err := fromFirestore(in, &out)
	if err != nil {
		t.Errorf("Should not return error: %v", err)
	}

	if out.ID.String() != uuid1.String() {
		t.Errorf("ID should be %s", uuid1)
	}

	if out.Nested.ID.String() != uuid2.String() {
		t.Errorf("Nested ID should be %s", uuid2)
	}
}
