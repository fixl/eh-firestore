module gitlab.com/fixl/eh-firestore

go 1.15

require (
	cloud.google.com/go v0.78.0 // indirect
	cloud.google.com/go/firestore v1.5.0
	github.com/google/uuid v1.2.0
	github.com/looplab/eventhorizon v0.11.0
	go.opencensus.io v0.23.0 // indirect
	golang.org/x/net v0.0.0-20210226172049-e18ecbb05110 // indirect
	golang.org/x/oauth2 v0.0.0-20210220000619-9bb904979d93 // indirect
	golang.org/x/sys v0.0.0-20210228012217-479acdf4ea46 // indirect
	google.golang.org/api v0.40.0
	google.golang.org/genproto v0.0.0-20210226172003-ab064af71705 // indirect
	google.golang.org/grpc v1.36.0
)
