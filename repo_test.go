// Copyright (c) 2021 - The Event Horizon Cloud Firestore authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package firestore

import (
	"context"
	"os"
	"reflect"
	"testing"
	"time"

	"cloud.google.com/go/firestore"

	eh "github.com/looplab/eventhorizon"
	"github.com/looplab/eventhorizon/mocks"
	"github.com/looplab/eventhorizon/repo"

	"github.com/google/uuid"
)

func TestRepo(t *testing.T) {
	if os.Getenv("FIRESTORE_EMULATOR_HOST") == "" {
		os.Setenv("FIRESTORE_EMULATOR_HOST", "127.0.0.1:8080")
	}

	r, err := NewRepo(uuid.New().String(), "mock.Model")
	if err != nil {
		t.Error("there should be no error:", err)
	}
	if r == nil {
		t.Error("there should be a repository")
	}

	r.SetEntityFactory(func() eh.Entity {
		return &mocks.Model{}
	})
	if r.Parent() != nil {
		t.Error("the parent repo should be nil")
	}
	repo.AcceptanceTest(t, context.Background(), r)
	extraRepoTests(context.Background(), t, r)

	// Repo with other namespace.
	ctx := eh.NewContextWithNamespace(context.Background(), "ns")
	repo.AcceptanceTest(t, ctx, r)
	extraRepoTests(ctx, t, r)
}

func extraRepoTests(ctx context.Context, t *testing.T, r *Repo) {
	// Insert a custom model
	modelCustom := &mocks.Model{
		ID:        uuid.New(),
		Content:   "ModelCustom",
		CreatedAt: time.Date(2019, time.March, 3, 12, 33, 0, 0, time.UTC),
	}
	if err := r.Save(ctx, modelCustom); err != nil {
		t.Error("there should be no error", err)
	}

	// FindCustom by content
	result, _ := r.FindCustom(ctx, func(ref *firestore.CollectionRef) *firestore.Query {
		q := ref.Where("content", "==", "ModelCustom")
		return &q
	})
	if len(result) != 1 {
		t.Error("there should be one item:", len(result))
	}
	if !reflect.DeepEqual(result[0], modelCustom) {
		t.Error("the item should be correct:", modelCustom)
	}

	// FindCustom with no query
	result, err := r.FindCustom(ctx, func(ref *firestore.CollectionRef) *firestore.Query {
		return nil
	})

	if rrErr, ok := err.(eh.RepoError); !ok || rrErr.Err != ErrInvalidQuery {
		t.Error("there should be a invalid query error:", err)
	}

	count := 0
	// FindCustom with query execution in the callback
	_, err = r.FindCustom(ctx, func(ref *firestore.CollectionRef) *firestore.Query {
		snap, _ := ref.Select().Snapshots(ctx).Next()
		count = snap.Size
		return nil
	})
	if rrErr, ok := err.(eh.RepoError); !ok || rrErr.Err != ErrInvalidQuery {
		t.Error("there should be a invalid query error:", err)
	}
	if count != 2 {
		t.Error("the count should be correct:", count)
	}

	// FindCustomIter by content
	iter, err := r.FindCustomIter(ctx, func(ref *firestore.CollectionRef) *firestore.Query {
		q := ref.Where("content", "==", "ModelCustom")
		return &q
	})
	if err != nil {
		t.Error("there should be no error:", err)
	}

	if iter.Next(context.Background()) != true {
		t.Error("iterator should have results")
	}
	if !reflect.DeepEqual(iter.Value(), modelCustom) {
		t.Error("the item should be correct:", modelCustom)
	}
	if iter.Next(context.Background()) == true {
		t.Error("the iterator should have no results")
	}
	err = iter.Close(context.Background())
	if err != nil {
		t.Error("there should be no error:", err)
	}
}
