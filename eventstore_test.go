// Copyright (c) 2021 - The Event Horizon Cloud Firestore authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package firestore

import (
	"context"
	"os"
	"testing"

	"github.com/google/uuid"

	eh "github.com/looplab/eventhorizon"
	"github.com/looplab/eventhorizon/eventstore"
)

func TestEventStore(t *testing.T) {
	if os.Getenv("FIRESTORE_EMULATOR_HOST") == "" {
		os.Setenv("FIRESTORE_EMULATOR_HOST", "127.0.0.1:8080")
	}

	store, err := NewEventStore(uuid.New().String(), "events")
	if err != nil {
		t.Fatal("there should be no error: ", err)
	}

	if store == nil {
		t.Fatal("there should be a store")
	}
	defer store.Close(context.Background())

	customNamespaceCtx := eh.NewContextWithNamespace(context.Background(), "ns")
	defer func() {
		if err := store.Clear(context.Background()); err != nil {
			t.Fatal("there should be no error:", err)
		}

		if err = store.Clear(customNamespaceCtx); err != nil {
			t.Fatal("there should be no error:", err)
		}
	}()

	// Run the actual test suite, both for default and custom namespaces
	eventstore.AcceptanceTest(t, context.Background(), store)
	eventstore.AcceptanceTest(t, customNamespaceCtx, store)
	eventstore.MaintainerAcceptanceTest(t, context.Background(), store)
}
