// Copyright (c) 2021 - The Event Horizon Cloud Firestore authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package firestore

import (
	"context"
	"errors"
	"strings"
	"time"

	"google.golang.org/api/iterator"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"github.com/google/uuid"

	eh "github.com/looplab/eventhorizon"

	"cloud.google.com/go/firestore"
	"google.golang.org/api/option"
)

var (
	// ErrCouldNotSaveAggregate is when an aggregate could not be saved.
	ErrCouldNotSaveAggregate = errors.New("could not save aggregate")
	// ErrCouldNotUnmarshalAggregate is when an aggregate could not be unmarshaled into a concrete type.
	ErrCouldNotUnmarshalAggregate = errors.New("could not unmarshal aggregate")
	// ErrCouldNotMarshalEvent is when an event could not be marshaled into JSON
	ErrCouldNotMarshalEvent = errors.New("could not marshal event")
	// ErrCouldNotUnmarshalEvent is when an event could not be unmarshaled into a concrete type.
	ErrCouldNotUnmarshalEvent = errors.New("could not unmarshal event")
)

// EventStore implements an EventStore for Firestore
type EventStore struct {
	client *firestore.Client
	prefix string
}

// NewEventStore creates a new EventStore with optional GCP connection settings
func NewEventStore(projectID, prefix string, opts ...option.ClientOption) (*EventStore, error) {
	ctx := context.Background()
	client, err := firestore.NewClient(ctx, projectID, opts...)
	if err != nil {
		return nil, err
	}

	return NewEventStoreWithClient(prefix, client), nil
}

// NewEventStoreWithClient creates a new EventStore with DB
func NewEventStoreWithClient(prefix string, client *firestore.Client) *EventStore {
	return &EventStore{
		client: client,
		prefix: prefix,
	}
}

// Save implements the Save method of the eventhorizon.EventStore interface
func (s *EventStore) Save(ctx context.Context, events []eh.Event, originalVersion int) error {
	if len(events) == 0 {
		return eh.EventStoreError{
			Err:       eh.ErrNoEventsToAppend,
			Namespace: eh.NamespaceFromContext(ctx),
		}
	}

	collectionName := s.collectionName(ctx)

	dbEvents := make([]evt, len(events))
	aggregateID := events[0].AggregateID()
	for i, event := range events {
		// Only accept events that belong to the same aggregate
		if event.AggregateID() != aggregateID {
			return eh.EventStoreError{
				Err:       eh.ErrInvalidEvent,
				Namespace: eh.NamespaceFromContext(ctx),
			}
		}

		// Only accept events that apply to the correct aggregate version
		if event.Version() != originalVersion+i+1 {
			return eh.EventStoreError{
				Err:       eh.ErrIncorrectEventVersion,
				Namespace: eh.NamespaceFromContext(ctx),
			}
		}

		// Crete the event record for  the DB.
		e, err := newEvt(ctx, event)
		if err != nil {
			return err
		}
		dbEvents[i] = *e
	}

	aggregateRef := s.client.Collection(collectionName).Doc(aggregateID.String())

	// Insert
	if originalVersion == 0 {
		aggregate := aggregateRecord{
			Version: len(dbEvents),
			Events:  dbEvents,
		}

		_, err := aggregateRef.Create(ctx, aggregate)
		if err != nil {
			return eh.EventStoreError{
				Err:       ErrCouldNotSaveAggregate,
				BaseErr:   err,
				Namespace: eh.NamespaceFromContext(ctx),
			}
		}

		return nil
	}

	// Update
	err := s.client.RunTransaction(ctx, func(ctx context.Context, tx *firestore.Transaction) error {
		// TODO: Does this do optimistic locking properly?
		aggregate, err := tx.Get(aggregateRef)
		if err != nil {
			return err
		}

		version, err := aggregate.DataAt("version")
		if err != nil {
			return err
		}

		err = tx.Update(aggregateRef, []firestore.Update{{Path: "version", Value: int(version.(int64)) + len(dbEvents)}})
		if err != nil {
			return err
		}

		_dbEvents := make([]interface{}, len(dbEvents))
		for i, e := range dbEvents {
			_dbEvents[i] = e
		}

		return tx.Update(aggregateRef, []firestore.Update{{Path: "events", Value: firestore.ArrayUnion(_dbEvents...)}})
	})

	if err != nil {
		return eh.EventStoreError{
			BaseErr:   err,
			Err:       ErrCouldNotSaveAggregate,
			Namespace: eh.NamespaceFromContext(ctx),
		}
	}

	return nil
}

// Load implements the Load method of the eventhorizon.EventStore interface.
func (s *EventStore) Load(ctx context.Context, id uuid.UUID) ([]eh.Event, error) {
	collectionName := s.collectionName(ctx)

	aggregateRef := s.client.Collection(collectionName).Doc(id.String())

	aggregate, err := aggregateRef.Get(ctx)
	if status.Code(err) == codes.NotFound {
		return []eh.Event{}, nil
	} else if err != nil {
		return nil, eh.EventStoreError{
			Err:       err,
			Namespace: eh.NamespaceFromContext(ctx),
		}
	}

	var record aggregateRecord
	if err = aggregate.DataTo(&record); err != nil {
		return nil, eh.EventStoreError{
			BaseErr:   err,
			Err:       ErrCouldNotUnmarshalAggregate,
			Namespace: eh.NamespaceFromContext(ctx),
		}
	}

	events := make([]eh.Event, len(record.Events))
	for i, e := range record.Events {
		if len(e.RawData) > 0 {
			var err error
			if e.data, err = eh.CreateEventData(e.EventType); err != nil {
				return nil, eh.EventStoreError{
					Err:       ErrCouldNotUnmarshalEvent,
					BaseErr:   err,
					Namespace: eh.NamespaceFromContext(ctx),
				}
			}
			if err = fromFirestore(e.RawData, e.data); err != nil {
				return nil, eh.EventStoreError{
					Err:       ErrCouldNotUnmarshalEvent,
					BaseErr:   err,
					Namespace: eh.NamespaceFromContext(ctx),
				}
			}
			e.RawData = nil
		}

		event := eh.NewEvent(
			e.EventType,
			e.data,
			e.Timestamp,
			eh.ForAggregate(
				e.AggregateType,
				e.AggregateID,
				e.Version,
			),
			eh.WithMetadata(e.MetaData),
		)
		events[i] = event
	}

	return events, nil
}

// Replace implements the Replace method of the eventhorizon.EventStore interface.
func (s *EventStore) Replace(ctx context.Context, event eh.Event) error {
	collectionName := s.collectionName(ctx)

	aggregateRef := s.client.Collection(collectionName).Doc(event.AggregateID().String())

	aggregate, err := aggregateRef.Get(ctx)
	if status.Code(err) == codes.NotFound {
		return eh.ErrAggregateNotFound
	} else if err != nil {
		return eh.EventStoreError{
			Err:       err,
			Namespace: eh.NamespaceFromContext(ctx),
		}
	}

	var record aggregateRecord
	if err = aggregate.DataTo(&record); err != nil {
		return eh.EventStoreError{
			BaseErr:   err,
			Err:       ErrCouldNotUnmarshalAggregate,
			Namespace: eh.NamespaceFromContext(ctx),
		}
	}

	e, err := newEvt(ctx, event)
	if err != nil {
		return err
	}

	found := false
	for i, dbEvent := range record.Events {
		if dbEvent.Version != event.Version() {
			continue
		}

		found = true

		// Switch out event
		record.Events[i] = *e
		break
	}

	if found == false {
		return eh.ErrInvalidEvent
	}

	// TODO: Make more efficient and don't replace whole aggregate
	_, err = aggregateRef.Set(ctx, record)
	if err != nil {
		return eh.EventStoreError{
			Err:       ErrCouldNotSaveAggregate,
			BaseErr:   err,
			Namespace: eh.NamespaceFromContext(ctx),
		}
	}

	return nil
}

// RenameEvent implements the RenameEvent method of the eventhorizon.EventStore interface.
func (s *EventStore) RenameEvent(ctx context.Context, from, to eh.EventType) error {
	collectionName := s.collectionName(ctx)

	// FIXME: Query for aggregates that have events of right type once firestore supports those queries
	it := s.client.Collection(collectionName).Documents(ctx)
	defer it.Stop()

	for {
		aggregate, err := it.Next()

		if err == iterator.Done {
			break
		}

		var record aggregateRecord
		if err = aggregate.DataTo(&record); err != nil {
			return eh.EventStoreError{
				BaseErr:   err,
				Err:       ErrCouldNotUnmarshalAggregate,
				Namespace: eh.NamespaceFromContext(ctx),
			}
		}

		hasChanges := false
		for i := range record.Events {
			if record.Events[i].EventType == from {
				record.Events[i].EventType = to
				hasChanges = true
			}
		}

		if hasChanges == false {
			continue
		}

		_, err = aggregate.Ref.Set(ctx, record)

		if err != nil {
			return eh.EventStoreError{
				BaseErr:   err,
				Err:       ErrCouldNotSaveAggregate,
				Namespace: eh.NamespaceFromContext(ctx),
			}
		}
	}

	return nil
}

func (s *EventStore) Clear(ctx context.Context) error {
	collectionName := s.collectionName(ctx)

	docRefs, err := s.client.Collection(collectionName).DocumentRefs(ctx).GetAll()
	if err != nil {
		return err
	}

	batch := s.client.Batch()
	for _, docRef := range docRefs {
		batch.Delete(docRef)
	}

	_, err = batch.Commit(ctx)

	return err
}

func (s *EventStore) Close(ctx context.Context) {
	s.client.Close()
}

// collectionName appends the namespace to the db prefix
func (s *EventStore) collectionName(ctx context.Context) string {
	ns := eh.NamespaceFromContext(ctx)
	return strings.Join([]string{s.prefix, ns, "eh_events"}, "_")
}

// aggregateRecord is the Database representation of an aggregate
type aggregateRecord struct {
	Version int   `firestore:"version"`
	Events  []evt `firestore:"events"`
}

// evt is the internal event record in firestore event store used to save
// and load events from the db
type evt struct {
	EventType     eh.EventType           `firestore:"event_type"`
	RawData       firestoreData          `firestore:"data,omitempty"`
	data          eh.EventData           `firestore:"-"`
	Timestamp     time.Time              `firestore:"timestamp"`
	AggregateType eh.AggregateType       `firestore:"aggregate_type"`
	AggregateID   uuid.UUID              `firestore:"aggregate_id"`
	Version       int                    `firestore:"version"`
	MetaData      map[string]interface{} `firestore:"metadata"`
}

// newEvt returns a new evt for and event
func newEvt(ctx context.Context, event eh.Event) (*evt, error) {
	rawData, err := toFirestore(event.Data())
	if err != nil {
		return nil, eh.EventStoreError{
			Err:       ErrCouldNotMarshalEvent,
			BaseErr:   err,
			Namespace: eh.NamespaceFromContext(ctx),
		}
	}
	return &evt{
		EventType:     event.EventType(),
		Timestamp:     event.Timestamp(),
		AggregateType: event.AggregateType(),
		AggregateID:   event.AggregateID(),
		Version:       event.Version(),
		MetaData:      event.Metadata(),

		RawData: rawData,
	}, nil
}
