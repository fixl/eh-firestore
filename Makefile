EH_VERSION = v0.11.0

COMPOSE_RUN_GO = docker-compose run --rm golang

prepare:
	docker-compose pull
.PHONY: prepare

init:
	$(COMPOSE_RUN_GO) make _init
.PHONY: init

upgrade:
	$(COMPOSE_RUN_GO) make _upgrade
.PHONY: upgrade

test:
	$(COMPOSE_RUN_GO) make _test
.PHONY: test

services:
	docker-compose up -d firestore
.PHONY: services

clean:
	docker-compose down --remove-orphans
.PHONY: clean

# Helpers
shellGo:
	$(COMPOSE_RUN_GO) sh
.PHONY: shelllGo

# Internal targets
_init:
	go mod download
	go mod tidy

_upgrade:
	go get -u -t ./...
	go get github.com/looplab/eventhorizon@$(EH_VERSION)
	go mod tidy

_test:
	go test ./... -cover

gitTag:
	-git tag -d $(EH_VERSION)
	-git push origin :refs/tags/$(EH_VERSION)
	git tag $(EH_VERSION)
	git push origin $(EH_VERSION)
