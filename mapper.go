// Copyright (c) 2021 - The Event Horizon Cloud Firestore authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package firestore

import (
	"encoding/json"
)

type firestoreData map[string]interface{}

// fromFirestore converts a map to the provided interface
func fromFirestore(from firestoreData, to interface{}) error {
	return convert(from, to)
}

// toFirestore converts a struct into a map an converts uuid.UUID values to strings
func toFirestore(from interface{}) (firestoreData, error) {
	var retValue firestoreData

	err := convert(from, &retValue)
	if err != nil {
		return nil, err
	}

	return retValue, nil
}

func convert(from, to interface{}) error {
	fromrec, _ := json.Marshal(from)
	return json.Unmarshal(fromrec, to)
}
