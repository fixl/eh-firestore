[![pipeline status](https://gitlab.com/fixl/eh-firestore/badges/master/pipeline.svg)](https://gitlab.com/fixl/eh-firestore/commits/master)
[![coverage report](https://gitlab.com/fixl/eh-firestore/badges/master/coverage.svg)](https://gitlab.com/fixl/eh-firestore/commits/master)
[![GoDoc](https://godoc.org/gitlab.com/fixl/eh-firestore?status.svg)](https://godoc.org/gitlab.com/fixl/eh-firestore)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/fixl/eh-firestore)](https://goreportcard.com/report/gitlab.com/fixl/eh-firestore)

# eh-firestore

eh-firestore contains a [Google Cloud Firestore] for the [Event Horizon] 
CQRS/ES toolkit for Go.

[Google Cloud Firestore]: https://cloud.google.com/firestore/docs/
[Event Horizon]: https://github.com/looplab/eventhorizon

# Usage

See the Event Horizon example folder for a few examples to get you started and
replace the storage drivers (event store and/or repo)

## Development

To develop eh-firestore you need to have Docker and Docker Compose installed.

To start all required services and run all tests, simply run make:

```bash
make
```

To manually start services for local development outside Docker:

```bash
make services
```

You can then run either one of the following:

```bash
make test
make _test
go test ./...
```

or use your favourite IDE to run your tests with a Cloud Firestore emulator
in the background.
