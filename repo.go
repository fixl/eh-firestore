// Copyright (c) 2021 - The Event Horizon Cloud Firestore authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package firestore

import (
	"context"
	"errors"
	"strings"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"google.golang.org/api/iterator"

	"github.com/google/uuid"
	"cloud.google.com/go/firestore"

	eh "github.com/looplab/eventhorizon"
	"google.golang.org/api/option"
)

// ErrCouldNotDialDB is when the database could not be dialed.
var ErrCouldNotDialDB = errors.New("could not dial database")

// ErrNoDBClient is when no database client is set.
var ErrNoDBClient = errors.New("no database client")

// ErrModelNotSet is when an model factory is not set on the Repo.
var ErrModelNotSet = errors.New("model not set")

// ErrInvalidQuery is when a query was not returned from the callback to FindCustom.
var ErrInvalidQuery = errors.New("invalid query")

// ErrCouldNotConvertData is when data could not be converted
var ErrCouldNotConvertData = errors.New("could not convert data")

const (
	// FieldCreated the name of the field tracking when an entity was created
	FieldCreated = "_created"

	// FieldUpdated the name of the field tracking when an entity was updated
	FieldUpdated = "_updated"
)

// Repo implements a Cloud Firestore repository for entities
type Repo struct {
	client     *firestore.Client
	collection string
	factoryFn  func() eh.Entity
}

// NewRepo created a new Repo.
func NewRepo(projectID, collection string, opts ...option.ClientOption) (*Repo, error) {
	ctx := context.Background()
	client, err := firestore.NewClient(ctx, projectID, opts...)
	if err != nil {
		return nil, ErrCouldNotDialDB
	}

	return NewRepoWithClient(collection, client)
}

// NewRepoWithClient created a new Repo with a client.
func NewRepoWithClient(collection string, client *firestore.Client) (*Repo, error) {
	if client == nil {
		return nil, ErrNoDBClient
	}
	return &Repo{
		client:     client,
		collection: collection,
	}, nil
}

// Parent implements the Parent method of the eventhorizon.ReadRepo interface
func (r *Repo) Parent() eh.ReadRepo {
	return nil
}

// Find implements the Find method of the eventhorizon.ReadRepo interface
func (r *Repo) Find(ctx context.Context, id uuid.UUID) (eh.Entity, error) {
	if r.factoryFn == nil {
		return nil, eh.RepoError{
			Err:       ErrModelNotSet,
			Namespace: eh.NamespaceFromContext(ctx),
		}
	}

	entityRef, err := r.client.Collection(r.collectionName(ctx)).Doc(id.String()).Get(ctx)
	if err != nil {
		return nil, eh.RepoError{
			Err: eh.ErrEntityNotFound,
			BaseErr: err,
			Namespace: eh.NamespaceFromContext(ctx),
		}
	}

	if entityRef.Exists() == false {
		return nil, eh.RepoError{
			Err:       eh.ErrEntityNotFound,
			BaseErr:   err,
			Namespace: eh.NamespaceFromContext(ctx),
		}
	}

	// Returns NotFound which we've already checked above
	entity, err := r.fromFirestore(entityRef)

	if err != nil {
		return nil, eh.RepoError{
			Err:       ErrCouldNotConvertData,
			BaseErr:   err,
			Namespace: eh.NamespaceFromContext(ctx),
		}
	}

	return entity, nil
}

// FindAll implements the FindAll method of the eventhorizon.ReadRepo interface
func (r *Repo) FindAll(ctx context.Context) ([]eh.Entity, error) {
	if r.factoryFn == nil {
		return nil, eh.RepoError{
			Err:       ErrModelNotSet,
			Namespace: eh.NamespaceFromContext(ctx),
		}
	}

	iter := r.client.Collection(r.collectionName(ctx)).OrderBy(FieldCreated, firestore.Asc).Documents(ctx)
	defer iter.Stop()

	var result []eh.Entity
	for {
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}

		entity, err := r.fromFirestore(doc)
		if err != nil {
			return nil, eh.RepoError{
				Err:       ErrCouldNotConvertData,
				BaseErr:   err,
				Namespace: eh.NamespaceFromContext(ctx),
			}
		}

		result = append(result, entity)
	}

	return result, nil
}

// This iterator is not thread safe.
type iter struct {
	iter *firestore.DocumentIterator
	data eh.Entity
	ff   func(*firestore.DocumentSnapshot) (eh.Entity, error)
}

func (i *iter) Next(ctx context.Context) bool {
	ref, err := i.iter.Next()
	if err != nil {
		return false
	}

	item, err := i.ff(ref)
	if err != nil {
		return false
	}

	i.data = item

	return true
}

func (i *iter) Value() interface{} {
	return i.data
}

func (i *iter) Close(ctx context.Context) error {
	i.iter.Stop()
	return nil
}

// FindCustomIter returns a curser you can use to stream results of very large datasets
func (r *Repo) FindCustomIter(ctx context.Context, callback func(*firestore.CollectionRef) *firestore.Query) (eh.Iter, error) {
	if r.factoryFn == nil {
		return nil, eh.RepoError{
			Err:       ErrModelNotSet,
			Namespace: eh.NamespaceFromContext(ctx),
		}
	}

	collection := r.client.Collection(r.collectionName(ctx))
	query := callback(collection)

	return &iter{
		iter: query.Documents(ctx),
		ff:   r.fromFirestore,
	}, nil
}

// FindCustom uses a callback to specify a custom query for returning models.
// It can also be used to do queries that do not map to the model by executing
// the query in the callback and returning nil to block the second execution of
// the same query in FindCustom. Expect a ErrInvalidQuery if returning a nil
// query from the callback
func (r *Repo) FindCustom(ctx context.Context, callback func(*firestore.CollectionRef) *firestore.Query) ([]interface{}, error) {
	if r.factoryFn == nil {
		return nil, eh.RepoError{
			Err:       ErrModelNotSet,
			Namespace: eh.NamespaceFromContext(ctx),
		}
	}

	collection := r.client.Collection(r.collectionName(ctx))
	query := callback(collection)

	if query == nil {
		return nil, eh.RepoError{
			Err:       ErrInvalidQuery,
			Namespace: eh.NamespaceFromContext(ctx),
		}
	}

	iter := query.Documents(ctx)
	defer iter.Stop()
	var result []interface{}

	for {
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}

		entity, err := r.fromFirestore(doc)
		if err != nil {
			return nil, eh.RepoError{
				Err:       ErrCouldNotConvertData,
				BaseErr:   err,
				Namespace: eh.NamespaceFromContext(ctx),
			}
		}

		result = append(result, entity)
	}

	return result, nil
}

// Save implements the Save method of the eventhorizon.WriteRepo interface.
func (r *Repo) Save(ctx context.Context, entity eh.Entity) error {
	if entity.EntityID() == uuid.Nil {
		return eh.RepoError{
			Err:       eh.ErrCouldNotSaveEntity,
			BaseErr:   eh.ErrMissingEntityID,
			Namespace: eh.NamespaceFromContext(ctx),
		}
	}

	converted, err := toFirestore(entity)
	if err != nil {
		return eh.RepoError{
			Err:       eh.ErrCouldNotSaveEntity,
			BaseErr:   err,
			Namespace: eh.NamespaceFromContext(ctx),
		}
	}
	converted[FieldCreated] = firestore.ServerTimestamp
	converted[FieldUpdated] = firestore.ServerTimestamp

	ref := r.client.Collection(r.collectionName(ctx)).Doc(entity.EntityID().String())

	// Create Entity
	_, err = ref.Create(ctx, converted)
	if err == nil {
		return nil
	}

	if status.Code(err) != codes.AlreadyExists {
		return eh.RepoError{
			Err:       eh.ErrCouldNotSaveEntity,
			BaseErr:   err,
			Namespace: eh.NamespaceFromContext(ctx),
		}
	}

	// Try update if create failed
	delete(converted, FieldCreated)
	if _, err := ref.Set(ctx, converted, firestore.MergeAll); err != nil {
		return eh.RepoError{
			Err:       eh.ErrCouldNotSaveEntity,
			BaseErr:   err,
			Namespace: eh.NamespaceFromContext(ctx),
		}
	}

	return nil
}

// Remove implements the Remove method of the eventhorizon.WriteRepo interface.
func (r *Repo) Remove(ctx context.Context, id uuid.UUID) error {
	ref := r.client.Collection(r.collectionName(ctx)).Doc(id.String())

	entityRef, err := ref.Get(ctx)
	if err != nil {
		return eh.RepoError{
			Err:       eh.ErrEntityNotFound,
			BaseErr:   err,
			Namespace: eh.NamespaceFromContext(ctx),
		}
	}

	if entityRef.Exists() == false {
		return eh.RepoError{
			Err:       eh.ErrEntityNotFound,
			BaseErr:   err,
			Namespace: eh.NamespaceFromContext(ctx),
		}
	}

	// Delete
	ref.Delete(ctx)

	return nil
}

// SetEntityFactory sets a factory function that creates concrete entity types.
func (r *Repo) SetEntityFactory(f func() eh.Entity) {
	r.factoryFn = f
}

func (r *Repo) collectionName(ctx context.Context) string {
	ns := eh.NamespaceFromContext(ctx)
	return strings.Join([]string{r.collection, ns}, "_")
}

// fromFirestore transforms document back to an eventhorizon.Entity
func (r *Repo) fromFirestore(snap *firestore.DocumentSnapshot) (eh.Entity, error) {
	entity := r.factoryFn()

	err := fromFirestore(snap.Data(), entity)
	if err != nil {
		return nil, err
	}

	return entity, nil
}
